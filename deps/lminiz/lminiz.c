#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#define MINIZ_NO_ZLIB_COMPATIBLE_NAMES
#include "miniz.c"

#define READER_METATABLE "lminiz reader"
#define WRITER_METATABLE "lminiz writer"

#define CHECK_READER(L) \
    ((struct lminiz_archive *) luaL_checkudata(L, 1, READER_METATABLE))

#define CHECK_WRITER(L) \
    ((struct lminiz_archive *) luaL_checkudata(L, 1, WRITER_METATABLE))

struct lminiz_archive
{
    mz_zip_archive archive;
};

int lminiz_reader_new_file(lua_State *L, const char *path)
{
    struct lminiz_archive *archive;

    archive = (struct lminiz_archive *) lua_newuserdata(L, sizeof(*archive));
    memset(archive, 0, sizeof(*archive));

    if (!mz_zip_reader_init_file(&archive->archive, path, 0))
    {
        lua_pop(L, 1);
        return 0;
    }

    luaL_getmetatable(L, READER_METATABLE);
    lua_setmetatable(L, -2);

    return 1;
}

static int lminiz_reader_new_file_l(lua_State *L)
{
    const char *path;

    path = luaL_checkstring(L, 1);
    luaL_argcheck(L, path != NULL, 1, "invalid path");

    if (lminiz_reader_new_file(L, path))
    {
        return 1;
    }

    lua_pushnil(L);
    lua_pushstring(L, "not a zip archive");

    return 2;
}

int lminiz_reader_new_mem(lua_State *L, void *p, size_t s)
{
    struct lminiz_archive *archive;

    archive = (struct lminiz_archive *) lua_newuserdata(L, sizeof(*archive));
    memset(archive, 0, sizeof(*archive));

    if (!mz_zip_reader_init_mem(&archive->archive, p, s, 0))
    {
        lua_pop(L, 1);
        return 0;
    }

    luaL_getmetatable(L, READER_METATABLE);
    lua_setmetatable(L, -2);

    return 1;
}

static int lminiz_reader_num_files(lua_State *L)
{
    struct lminiz_archive *reader;
    mz_uint n;
    reader = CHECK_READER(L);
    n = mz_zip_reader_get_num_files(&reader->archive);
    lua_pushinteger(L, n);
    return 1;
}

static int lminiz_reader_stat(lua_State *L)
{
    struct lminiz_archive *reader;
    lua_Integer index;
    lua_Integer nfiles;
    mz_zip_archive_file_stat sb;
    mz_uint filename_len;
    char *filename;

    reader = CHECK_READER(L);

    index = luaL_checkinteger(L, 2);
    nfiles = mz_zip_reader_get_num_files(&reader->archive);
    luaL_argcheck(L, index >= 0 && index < nfiles, 2, "invalid file index");

    if (!mz_zip_reader_file_stat(&reader->archive, index, &sb))
    {
        return luaL_error(L, "stat failed");
    }

    lua_newtable(L);

    lua_pushinteger(L, sb.m_file_index);
    lua_setfield(L, -2, "file_index");

    lua_pushinteger(L, sb.m_crc32);
    lua_setfield(L, -2, "crc32");

    lua_pushinteger(L, sb.m_comp_size);
    lua_setfield(L, -2, "comp_size");

    lua_pushinteger(L, sb.m_uncomp_size);
    lua_setfield(L, -2, "uncomp_size");

    filename_len = mz_zip_reader_get_filename(&reader->archive, index, NULL,
        0);
    filename = malloc(filename_len);
    mz_zip_reader_get_filename(&reader->archive, index, filename, filename_len);
    lua_pushlstring(L, filename, filename_len-1);
    lua_setfield(L, -2, "filename");
    free(filename);

    lua_pushinteger(L, sb.m_internal_attr);
    lua_setfield(L, -2, "internal_attr");

    lua_pushinteger(L, sb.m_external_attr);
    lua_setfield(L, -2, "external_attr");

    return 1;
}

static int lminiz_reader_is_directory(lua_State *L)
{
    struct lminiz_archive *reader;
    mz_zip_archive *archive;
    mz_uint index, nfiles;

    reader = CHECK_READER(L);
    archive = &reader->archive;

    index = luaL_checkinteger(L, 2);
    nfiles = mz_zip_reader_get_num_files(&reader->archive);
    luaL_argcheck(L, index >= 0 && index < nfiles, 2, "invalid file index");

    lua_pushboolean(L, mz_zip_reader_is_file_a_directory(archive, index));

    return 1;
}

static int lminiz_reader_extract(lua_State *L)
{
    struct lminiz_archive *reader;
    mz_zip_archive *archive;
    mz_uint index, nfiles;
    mz_zip_archive_file_stat sb;
    void *mem;
    luaL_Buffer buf;

    reader = CHECK_READER(L);
    archive = &reader->archive;

    index = luaL_checkinteger(L, 2);

    nfiles = mz_zip_reader_get_num_files(archive);
    luaL_argcheck(L, index >= 0 && index < nfiles, 2, "invalid file index");

    if (!mz_zip_reader_file_stat(&reader->archive, index, &sb))
    {
        return luaL_error(L, "stat failed");
    }

    mem = luaL_buffinitsize(L, &buf, sb.m_uncomp_size);

    if (!mz_zip_reader_extract_to_mem(archive, index, mem, sb.m_uncomp_size, 0))
    {
        return luaL_error(L, "failed to extract file");
    }

    luaL_pushresultsize(&buf, sb.m_uncomp_size);

    return 1;
}

static int lminiz_reader_find(lua_State *L)
{
    struct lminiz_archive *reader;
    mz_zip_archive *archive;
    const char *path;
    int index;

    reader = CHECK_READER(L);
    archive = &reader->archive;

    path = luaL_checkstring(L, 2);

    index = mz_zip_reader_locate_file(archive, path, NULL, 0);

    if (index == -1)
    {
        lua_pushnil(L);
        return 1;
    }

    lua_pushinteger(L, index);
    return 1;
}

static int lminiz_reader_close(lua_State *L)
{
    struct lminiz_archive *reader;
    mz_zip_archive *archive;

    reader = CHECK_READER(L);
    archive = &reader->archive;

    if (archive->m_zip_mode != MZ_ZIP_MODE_INVALID)
    {
        mz_zip_reader_end(archive);
    }

    return 0;
}

static int lminiz_writer_new(lua_State *L)
{
    struct lminiz_archive *writer;
    size_t reserve, initalloc;

    reserve = luaL_optinteger(L, 1, 0);
    initalloc = luaL_optinteger(L, 2, 128*1024);

    writer = (struct lminiz_archive *) lua_newuserdata(L, sizeof(*writer));
    memset(writer, 0, sizeof(*writer));

    if (!mz_zip_writer_init_heap(&writer->archive, reserve, initalloc))
    {
        lua_pop(L, 1);
        lua_pushnil(L);
        lua_pushliteral(L, "mz_zip_writer_init_heap() failed");
        return 2;
    }

    luaL_getmetatable(L, WRITER_METATABLE);
    lua_setmetatable(L, -2);

    return 1;
}

static int lminiz_writer_add_from_reader(lua_State *L)
{
    struct lminiz_archive *writer, *reader;
    mz_uint idx;

    writer = CHECK_WRITER(L);
    reader = luaL_checkudata(L, 2, READER_METATABLE);

    idx = luaL_checkinteger(L, 3);

    if (!mz_zip_writer_add_from_zip_reader(&writer->archive, &reader->archive,
        idx))
    {
        return luaL_error(L, "failed to add file");
    }

    return 0;
}

static int lminiz_writer_add_buffer(lua_State *L)
{
    struct lminiz_archive *writer;
    const char *name;
    const char *buf;
    size_t bufsz;

    writer = CHECK_WRITER(L);
    name = luaL_checkstring(L, 2);
    buf = luaL_checklstring(L, 3, &bufsz);

    if (!mz_zip_writer_add_mem(&writer->archive, name, buf, bufsz,
        MZ_DEFAULT_COMPRESSION))
    {
        return luaL_error(L, "failed to add data");
    }

    return 0;
}

static int lminiz_writer_close(lua_State *L)
{
    struct lminiz_archive *writer;
    void *buf;
    size_t bufsz;

    writer = CHECK_WRITER(L);

    if (writer->archive.m_zip_mode == MZ_ZIP_MODE_INVALID)
    {
        lua_pushnil(L);
        return 1;
    }

    if (!mz_zip_writer_finalize_heap_archive(&writer->archive, &buf, &bufsz))
    {
        return luaL_error(L, "failed to finalize archive");
    }

    mz_zip_writer_end(&writer->archive);

    lua_pushlstring(L, buf, bufsz);

    return 1;
}

static int lminiz_writer_gc(lua_State *L)
{
    struct lminiz_archive *writer;
    void *buf;
    size_t bufsz;

    writer = CHECK_WRITER(L);

    if (writer->archive.m_zip_mode != MZ_ZIP_MODE_INVALID)
    {
        mz_zip_writer_end(&writer->archive);
    }

    return 0;
}

static const struct luaL_Reg reader_methods[] =
{
    { "num_files", lminiz_reader_num_files },
    { "stat", lminiz_reader_stat },
    { "is_directory", lminiz_reader_is_directory },
    { "extract", lminiz_reader_extract },
    { "find", lminiz_reader_find },
    { "close", lminiz_reader_close },
    { "__gc", lminiz_reader_close },
    { NULL, NULL },
};

static const struct luaL_Reg writer_methods[] =
{
    { "add_from_reader", lminiz_writer_add_from_reader },
    { "add_buffer", lminiz_writer_add_buffer },
    { "close" , lminiz_writer_close },
    { "__gc", lminiz_writer_gc },
    { NULL, NULL },
};

static const struct luaL_Reg lminiz_functions[] =
{
    { "reader", lminiz_reader_new_file_l },
    { "writer", lminiz_writer_new },
    { NULL, NULL },
};

int luaopen_lminiz(lua_State *L)
{
    luaL_newmetatable(L, READER_METATABLE);

    /* mt.__index = mt */
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");

    luaL_setfuncs(L, reader_methods, 0);

    lua_pop(L, 1);

    luaL_newmetatable(L, WRITER_METATABLE);

    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");

    luaL_setfuncs(L, writer_methods, 0);

    lua_pop(L, 1);

    luaL_newlib(L, lminiz_functions);

    return 1;
}
