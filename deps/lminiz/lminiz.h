#ifndef LMINIZ_H
#define LMINIZ_H

#if defined(__cplusplus)
extern "C" {
#endif

int luaopen_lminiz(lua_State *L);
int lminiz_reader_new_file(lua_State *L, const char *path);
int lminiz_reader_new_mem(lua_State *L, void *p, size_t s);

#if defined(__cplusplus)
}
#endif

#endif
