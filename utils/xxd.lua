local input = assert(io.open(arg[1], "rb"))
local output = assert(io.open(arg[2], "wb"))

local blocksize = 1024

local varname = string.reverse(arg[1])
local slash_idx, _ = string.find(varname, "/", 1, true)
if slash_idx then
    varname = string.sub(varname, 1, slash_idx-1)
end

varname = string.gsub(string.reverse(varname), "[^%w]", "_")

local linelen = 0
local inputsize = 0

output:write("const char ", varname, "_data[] = {\n")

while true do
    local bytes = input:read(blocksize)
    if not bytes then break end
    inputsize = inputsize + #bytes
    for i = 1, #bytes do
        output:write(string.byte(bytes, i), ",")
        -- if i < #bytes then output:write(",") end
        linelen = linelen + 1
        if linelen == 20 then
            linelen = 0
            output:write("\n")
        end
    end
end

output:write("};\n")
output:write("const unsigned int ", varname, "_size = ", inputsize, ";\n")
