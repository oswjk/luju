#ifndef LUJU_MANIFEST_H
#define LUJU_MANIFEST_H

#include <stdint.h>

static const uint32_t LUJU_MAGIC = 0xDEADBEEF;

struct luju_manifest
{
    uint32_t magic;
    uint32_t exe_size;
    uint32_t payload_size;
};

int get_luju_manifest(struct luju_manifest *manifest);

#endif
