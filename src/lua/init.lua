local arg = ...
local luju = require("luju")

-- Load the searcher module manually from resources, as we don't yet have the
-- functionality it contains :-)
local resources = luju.resources
local search_lua_idx = resources:find("lib/luju/search.lua")
local search = load(resources:extract(search_lua_idx), "search.lua")()

-- Put it also to package.loaded, so require does not need to call our searcher
-- to search the search :-)
package.loaded["luju.search"] = search

-- Register the resource searcher so we can require stuff from our resource
-- file.
search.register_searcher(search.resource_searcher)

if luju.payload then
    local run = require("luju.run")
    return run.execute_payload("payload.zip", luju.payload)
else
    local getopt = require("alt_getopt")
    local lfs = require("lfs")
    local lminiz = require("lminiz")
    local build = require("luju.build")

    local long_options = {
        build = "b",
        output = "o",
        payload = "p",
        run = "r",
    }

    local optarg, optind = getopt.get_opts(arg, "bo:p:r", long_options)

    if optarg.b then
        if not optarg.p then
            error("--payload is required")
        end

        if not optarg.o then
            error("--output is required")
        end

        local attrs, err = lfs.attributes(optarg.p)

        if not attrs then
            error(err)
        end

        if attrs.mode == "file" then
            local reader, errmsg = lminiz.reader(optarg.p)
            if not reader then
                print(errmsg)
            end
            reader:close()
            build.create_exe_from_file(optarg.p, optarg.o)
        elseif attrs.mode == "directory" then
            build.zip_directory(optarg.p, optarg.o .. "-payload.zip")
            build.create_exe_from_file(optarg.o .. "-payload.zip", optarg.o)
        else
            error("regular file or directory required")
        end
    elseif optarg.r then
        if not optarg.p then
            error("--payload is required")
        end

        local mode, err = lfs.attributes(optarg.p, "mode")

        if not mode then
            error(err)
        end

        if mode == "file" then
            local payload = lminiz.reader(optarg.p)
            return run.execute_payload(optarg.p, payload)
        elseif mode == "directory" then
            dirsep = package.config:sub(1, 1)
            pathsep = package.config:sub(3, 3)
            subst = package.config:sub(5, 5)

            local function searcher(name)
                local paths = {
                    optarg.p .. dirsep .. subst .. ".lua",
                    optarg.p .. dirsep .. subst .. dirsep .. "init.lua",
                }

                paths = table.concat(paths, pathsep)

                local modpath, errmsg = package.searchpath(name, paths)

                if errmsg then
                    return errmsg
                end

                return loadfile(modpath)
            end

            search.register_searcher(searcher)

            return dofile(optarg.p .. dirsep .. "main.lua")
        end
    end
end
