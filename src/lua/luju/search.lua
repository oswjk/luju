-- @module luju.search
local search = {}

local luju = require("luju")

-- try to emulate package.searchpath, but for lminiz instead. Returns a string
-- on error and an integer on success.
function search.lminiz_reader_searchpath(name, path, zip_name, reader)
    name = string.gsub(name, "%.", "/")
    local errmsg = ""

    for template in string.gmatch(path, "[^;]+") do
        local path = string.gsub(template, "%?", name)
        local idx = reader:find(path)
        if idx then
            return idx
        end
        errmsg = errmsg .. "\n\tno file '" .. zip_name .. "/" .. path .. "'"
    end

    return errmsg
end

-- Search for a lua module inside a zip file. Tries directly on the root of
-- the zip and in lib subdirectory.
function search.lminiz_searcher(name, zip_name, reader)
    local idx = search.lminiz_reader_searchpath(name, "?.lua;lib/?.lua",
        zip_name, reader)

    if type(idx) == "string" then
        return idx
    end

    local attrs = reader:stat(idx)

    local chunk, errmsg = load(reader:extract(idx),
        zip_name .. "/" .. attrs.filename)

    if not chunk then
        error(errmsg)
    end

    return chunk
end

-- Search files in the luju resource zip
function search.resource_searcher(name)
    return search.lminiz_searcher(name, "resources.zip", luju.resources)
end

-- Register a module searcher function to package.searchers.
function search.register_searcher(searcher)
    package.searchers[#package.searchers + 1] = searcher
end

return search
