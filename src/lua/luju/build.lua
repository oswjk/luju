-- @module luju.build
local build = {}

local lfs = require("lfs")
local luju = require("luju")
local lminiz = require("lminiz")

-- Writes a luju manifest to the end of a file.
function build.write_luju_manifest(file, exesize, payloadsize)
    file:write(string.pack("I4I4I4", 0xDEADBEEF, exesize, payloadsize))
end

-- Copy the src file to the dst file.
local function copyfile(src, dst)
    local blocksize = 1024*16
    while true do
        local data = src:read(blocksize)
        if not data then break end
        dst:write(data)
    end
end

-- Creates a standalone executable by appending a zip file as payload to the
-- luju executable and writing a manifest to the end.
function build.create_exe_from_file(srcpath, dstpath)
    local exepath = luju.get_executable_path()
    local exesize = lfs.attributes(exepath, "size")
    local srcsize = lfs.attributes(srcpath, "size")

    print("creating standalone executable at " .. dstpath)
    print("base executable size is " .. exesize)
    print("payload size is " .. srcsize)

    local output = assert(io.open(dstpath, "wb"))

    local input = assert(io.open(exepath, "rb"))
    copyfile(input, output)
    input:close()

    local input = assert(io.open(srcpath, "rb"))
    copyfile(input, output)
    input:close()

    build.write_luju_manifest(output, exesize, srcsize)

    output:close()
end

function build.zip_directory(path, dstpath)
    local writer = lminiz.writer()

    path = string.gsub(path, "%/$", "")

    local function filelisting(path)
        local files = {}
        for file in lfs.dir(path) do
            if file ~= "." and file ~= ".." then
                local fullpath = path .. "/" .. file
                local mode = lfs.attributes(fullpath, "mode")
                if mode == "file" then
                    files[#files+1] = fullpath
                elseif mode == "directory" then
                    local tmp = filelisting(fullpath)
                    for _, v in ipairs(tmp) do
                        files[#files+1] = v
                    end
                end
            end
        end
        return files
    end

    local files = filelisting(path)

    for i, v in ipairs(files) do
        local zipname = string.sub(v, string.len(path)+2)
        local input = io.open(v, "rb")
        local data = input:read("a")
        input:close()
        writer:add_buffer(zipname, data)
    end

    local archive = writer:close()

    local output = io.open(dstpath, "wb")
    output:write(archive)
    output:close()
end

return build
