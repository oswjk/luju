local windows = {}
local alien = require("alien")
local kernel32 = alien.load("kernel32")

windows.MultiByteToWideChar = kernel32.MultiByteToWideChar
windows.MultiByteToWideChar:types("int", "uint", "ulong", "string", "int", "pointer",
    "int")

windows.WideCharToMultiByte = kernel32.WideCharToMultiByte
windows.WideCharToMultiByte:types("int", "uint", "ulong", "pointer", "int", "pointer",
    "int", "pointer", "pointer")

windows.CP_UTF8 = 65001

function windows.array_wide(length)
    return alien.array("ushort", length)
end

-- Convert a utf-8 encoded lua string to a wide character alien array. Returns
-- the array and length of the wide string (not including NULL). The array is
-- always NULL terminated-
function windows.utf8_to_wide(str)
    assert(type(str) == "string", "string expected")

    local widelen = windows.MultiByteToWideChar(windows.CP_UTF8, 0, str,
        #str, nil, 0)

    assert(widelen ~= 0xFFFD, "invalid utf-8")

    local wide = windows.array_wide(widelen+1)

    windows.MultiByteToWideChar(windows.CP_UTF8, 0, str, #str, wide.buffer,
        widelen)

    wide[widelen+1] = 0

    return wide, widelen
end

-- Convert either an alien array or alien buffer containing wide chararacter
-- data to an utf-8 encoded lua string. `len` can be -1 if the wide string is
-- null terminated. If the wide string is null terminated, the null is not
-- included in the resulting utf-8 string.
function windows.wide_to_utf8(buf, len)
    if type(buf) == "table" then
        buf = buf.buffer
    end

    if not len then
        len = -1
    end

    local utf8len = windows.WideCharToMultiByte(windows.CP_UTF8, 0, buf, len,
        nil, 0, nil, nil)

    assert(utf8len ~= 0xFFFD, "invalid wide string")

    if utf8len == 0 then return "" end

    local utf8 = alien.buffer(utf8len)

    windows.WideCharToMultiByte(windows.CP_UTF8, 0, buf, len, utf8, utf8len,
        nil, nil)

    if len == -1 or buf[len] == 0 then
        utf8len = utf8len - 1
    end

    return utf8:tostring(utf8len)
end

return windows
