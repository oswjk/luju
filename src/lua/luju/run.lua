-- @module luju.run
local run = {}

local search = require("luju.search")

-- Executes a payload from zip file, using main.lua as entry point
function run.execute_payload(payloadname, payload)
    local i = payload:find("main.lua");

    if not i then
        error("no main.lua found in " .. payloadname)
    end

    local function searcher(name)
        return search.lminiz_searcher(name, payloadname, payload)
    end

    search.register_searcher(searcher)

    local main = load(payload:extract(i), "main.lua")

    return main(arg)
end

return run
