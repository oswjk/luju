#include "manifest.h"
#include "platform.h"

#include <stdio.h>
#include <stdlib.h>

int get_luju_manifest(struct luju_manifest *manifest)
{
    char *path = get_executable_path();
    if (!path)
    {
        return -1;
    }

    FILE *fp = fopen(path, "rb");
    if (!fp)
    {
        free(path);
        return -1;
    }

    if (fseek(fp, -sizeof(*manifest), SEEK_END) != 0)
    {
        free(path);
        fclose(fp);
        return -1;
    }

    if (fread(&manifest->magic, sizeof(manifest->magic), 1, fp) != 1)
    {
        goto read_error;
    }

    if (fread(&manifest->exe_size, sizeof(manifest->exe_size), 1, fp) != 1)
    {
        goto read_error;
    }

    if (fread(&manifest->payload_size, sizeof(manifest->payload_size), 1, fp) != 1)
    {
        goto read_error;
    }

    fclose(fp);
    free(path);

    return 0;

read_error:

    fclose(fp);
    free(path);

    return -1;
}
