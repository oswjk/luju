#include "platform.h"
#include <stdlib.h>

char *get_executable_path()
{
    char *buffer = NULL;
    size_t bufsz = 64;
    ssize_t rc;

    for (;;)
    {
        buffer = realloc(buffer, bufsz);
        rc = readlink("/proc/self/exe", buffer, bufsz);
        if (rc < 0)
        {
            free(buffer);
            return NULL;
        }
        else if (rc == bufsz)
        {
            bufsz *= 2;
            continue;
        }
        break;
    }

    buffer[rc] = '\0';

    return buffer;
}

void *mmap_executable(off_t offset, size_t length)
{
    char *path;
    int rc;
    int fd;
    void *mem;
    off_t pa_offset;

    path = get_executable_path();
    if (!path)
    {
        return NULL;
    }

    fd = open(path, O_RDONLY);
    free(path);
    if (fd < 0)
    {
        return NULL;
    }

    /* offset for mmap must be page aligned */
    pa_offset = offset & ~(sysconf(_SC_PAGE_SIZE) - 1);

    mem = mmap(NULL, length + offset - pa_offset, PROT_READ, MAP_SHARED, fd,
        pa_offset);
    if (!mem)
    {
        close(fd);
        return NULL;
    }

    close(fd);

    return mem + offset - pa_offset;
}
