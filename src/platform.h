#ifndef LUJU_PLATFORM_H
#define LUJU_PLATFORM_H

#include <errno.h>

#ifndef _WIN32
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#else
#include <stdint.h>
typedef int64_t off_t;
#endif

char *get_executable_path();
void *mmap_executable(off_t offset, size_t length);

#endif
