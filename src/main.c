#include "config.h"
#include "platform.h"
#include "manifest.h"

#include <stdio.h>
#include <stdlib.h>

/* lua */
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

/* lminiz */
#include <lminiz.h>

#ifdef ENABLE_LUAFILESYSTEM
/* LuaFileSystem */
#include <lfs.h>
#endif

#ifdef ENABLE_LUASOCKET
/* luasocket */
#include <luasocket.h>
#include <mime.h>
#endif

#ifdef ENABLE_SQLITE3
/* lsqlite3 */
LUALIB_API int luaopen_lsqlite3(lua_State *L);
#endif

#ifdef ENABLE_LPEG
/* lpeg */
int luaopen_lpeg (lua_State *L);
#endif

#ifdef ENABLE_ALIEN
int luaopen_alien_c(lua_State *L);
#endif

extern char init_lua_data[];
extern unsigned int init_lua_size;

extern char resources_zip_data[];
extern unsigned int resources_zip_size;

static int l_get_executable_path(lua_State *L)
{
    char *path = get_executable_path();
    lua_pushstring(L, path);
    free(path);
    return 1;
}

static int luaopen_luju(lua_State *L)
{
    lua_newtable(L);

    lua_pushcfunction(L, l_get_executable_path);
    lua_setfield(L, -2, "get_executable_path");

    lua_pushinteger(L, LUJU_MAGIC);
    lua_setfield(L, -2, "LUJU_MAGIC");

    return 1;
}

lua_State *create_lua_state()
{
    lua_State *L;

    L = luaL_newstate();

    if (!L)
    {
        fprintf(stderr, "luaL_newstate failed\n");
        return NULL;
    }

    luaL_openlibs(L);

    lua_getglobal(L, "package");

    lua_getfield(L, -1, "loaded");

    luaopen_luju(L);
    lua_setfield(L, -2, "luju");

    luaopen_lminiz(L);
    lua_setfield(L, -2, "lminiz");

    // pop loaded
    lua_pop(L, 1);

    lua_getfield(L, -1, "preload");

#ifdef ENABLE_SQLITE3
    lua_pushcfunction(L, luaopen_lsqlite3);
    lua_setfield(L, -2, "lsqlite3");
#endif

#ifdef ENABLE_LUAFILESYSTEM
    lua_pushcfunction(L, luaopen_lfs);
    lua_setfield(L, -2, "lfs");
#endif

#ifdef ENABLE_LUASOCKET
    lua_pushcfunction(L, luaopen_socket_core);
    lua_setfield(L, -2, "socket.core");

    lua_pushcfunction(L, luaopen_mime_core);
    lua_setfield(L, -2, "mime.core");
#endif

#ifdef ENABLE_LPEG
    lua_pushcfunction(L, luaopen_lpeg);
    lua_setfield(L, -2, "lpeg");
#endif

#ifdef ENABLE_ALIEN
    lua_pushcfunction(L, luaopen_alien_c);
    lua_setfield(L, -2, "alien_c");
#endif

    // pop preload and package
    lua_pop(L, 2);

    return L;
}

int setup_resources(lua_State *L, struct luju_manifest *manifest)
{
    lua_getglobal(L, "package");
    lua_getfield(L, -1, "loaded");
    lua_getfield(L, -1, "luju");

    if (!lminiz_reader_new_mem(L, resources_zip_data, resources_zip_size))
    {
        fprintf(stderr, "failed to read resources.zip\n");
        lua_pop(L, 3);
        return -1;
    }

    lua_setfield(L, -2, "resources");
    lua_pop(L, 3);

    return 0;
}

int setup_payload(lua_State *L, struct luju_manifest *manifest)
{
    if (manifest->payload_size == 0)
    {
        return -1;
    }
    else
    {
        void *payload = mmap_executable(manifest->exe_size,
            manifest->payload_size);

        if (!payload)
        {
            fprintf(stderr, "error: failed to mmap payload\n");
            return -1;
        }

        lua_getglobal(L, "package");
        lua_getfield(L, -1, "loaded");
        lua_getfield(L, -1, "luju");

        if (!lminiz_reader_new_mem(L, payload, manifest->payload_size))
        {
            fprintf(stderr, "error: failed to read payload\n");
            lua_pop(L, 3);
            return -1;
        }

        lua_setfield(L, -2, "payload");
        lua_pop(L, 3);
    }

    return 0;
}

int main(int argc, char **argv)
{
    int rc;
    lua_State *L;
    struct luju_manifest manifest;
    int index;
    int ret = 0;

    L = create_lua_state();
    if (!L)
    {
        return 1;
    }

    setup_resources(L, &manifest);

    if (get_luju_manifest(&manifest) == -1)
    {
        fprintf(stderr, "error: failed to read manifest\n");
        return 1;
    }

    if (manifest.magic == LUJU_MAGIC)
    {
        setup_payload(L, &manifest);
    }

    // Load the init.lua script
    if (luaL_loadbuffer(L, init_lua_data, init_lua_size, "init.lua"))
    {
      fprintf(stderr, "%s\n", lua_tostring(L, -1));
      return -1;
    }

    // Pass the command-line arguments to init as a zero-indexed table
    lua_createtable (L, argc, 0);
    for (index = 0; index < argc; index++)
    {
      lua_pushstring(L, argv[index]);
      lua_rawseti(L, -2, index);
    }

    // Start the init script
    if (lua_pcall(L, 1, 1, 0))
    {
      fprintf(stderr, "%s\n", lua_tostring(L, -1));
      return -1;
    }

    if (lua_isnumber(L, -1))
    {
        ret = lua_tointeger(L, -1);
    }

    lua_close(L);

    return ret;
}
