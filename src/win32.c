#include "platform.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <windows.h>

static char *wide_to_utf8(const wchar_t *s, int len)
{
    int need = WideCharToMultiByte(CP_UTF8, 0, s, len, NULL, 0, NULL, NULL);
    if (need == 0xFFFD)
    {
        return NULL;
    }
    char *buffer = malloc(need+1);
    need = WideCharToMultiByte(CP_UTF8, 0, s, len, buffer, need, NULL, NULL);
    buffer[need] = '\0';
    return buffer;
}

static wchar_t *utf8_to_wide(const char *s, int len)
{
    int need = MultiByteToWideChar(CP_UTF8, 0, s, len, NULL, 0);
    if (need = 0xFFFD)
    {
        return NULL;
    }
    wchar_t *buffer = malloc(sizeof(wchar_t) * (need+1));
    need = MultiByteToWideChar(CP_UTF8, 0, s, len, buffer, need);
    buffer[need] = '\0';
    return buffer;
}

wchar_t *get_executable_path_w()
{
    DWORD rc;
    wchar_t *buffer = NULL;
    int bufsz = 1024;

    for (;;)
    {
        buffer = realloc(buffer, bufsz * sizeof(wchar_t));
        rc = GetModuleFileNameW(NULL, buffer, bufsz);
        if (rc == bufsz)
        {
            bufsz *= 2;
            continue;
        }
        else if (rc == 0)
        {
            free(buffer);
            return NULL;
        }
        break;
    }

    buffer[rc] = L'\0';

    return buffer;
}

char *get_executable_path()
{
    wchar_t *wpath = get_executable_path_w();
    char *path = wide_to_utf8(wpath, -1);
    free(wpath);
    return path;
}

void *mmap_executable(off_t offset, size_t length)
{
    wchar_t *path;
    HANDLE hfile;
    HANDLE hfmo;
    void *mem;
    SYSTEM_INFO sysinfo;
    DWORD offsetlo, offsethi;
    uint64_t pa_offset;

    path = get_executable_path_w();
    if (!path)
    {
        return NULL;
    }

    hfile = CreateFileW(path, GENERIC_READ, FILE_SHARE_READ, NULL,
        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    // No need for the path anymore
    free(path);

    if (hfile == INVALID_HANDLE_VALUE)
    {
        return NULL;
    }

    hfmo = CreateFileMappingW(hfile, NULL, PAGE_READONLY, 0, 0, NULL);
    if (hfmo == NULL)
    {
        CloseHandle(hfile);
        return NULL;
    }

    GetSystemInfo(&sysinfo);

    // align the offset
    pa_offset = (uint64_t) offset & ~(sysinfo.dwAllocationGranularity - 1);
    offsethi = (pa_offset >> 32) & 0xFFFFFFFFul;
    offsetlo = pa_offset & 0xFFFFFFFFul;

    mem = MapViewOfFile(hfmo, FILE_MAP_READ, offsethi, offsetlo,
        length + offset - pa_offset);

    // It should be ok to close these. The view holds a reference to the
    // file mapping object and after unmapping, the references should be
    // freed.
    CloseHandle(hfmo);
    CloseHandle(hfile);

    if (!mem)
    {
        return NULL;
    }

    return mem + offset - pa_offset;
}
